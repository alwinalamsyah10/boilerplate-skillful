import crypto from 'crypto';
import { Op, where } from 'sequelize';
import { sequelize } from '../models';
import validator from 'validator';
import moment, { duration } from 'moment';
import initialHelper from './util/initialization.class';
import misc from '../misc/misc';


require('dotenv').config();

const {
  user: dbUser,
  merchant: dbMerchant,
} = require('../models/index');

class User {
  constructor(userId) {
    this.userId = userId;
  }

  async findUserById() {
    const userData = await dbUser
      .findOne({
        where: {
          user_id: this.userId,
        },
        // include: [
        //   {
        //     model: dbMerchant
        //   }
        // ]
      })
      .catch((err) => {
        throw new Error(err);
      });

    return userData;
  }

  static async getUserDataJWT(data, res) {
    const response = new Response(res);
    let dataUser;
    try {
      dataUser = await initialHelper.getUserJWT(data);
    } catch (error) {
      response.forbiddenRequest();
      throw new Error('Forbidden request');
    }

    return dataUser;
  }
  static async userAge(userId) {
    const userData = await dbUser
      .findOne({
        where: {
          user_id: userId,
        },
      })
      .catch((err) => {
        throw new Error(err);
      });
    const userAge = moment(userData.birth_date).format('YYYY-MM-DD');
    const age = moment.duration(moment().diff(userAge));
    return age.years().toString();
  }

  static async checkDuplicateEmail(email) {
    const userData = dbUser.findOne({
      where: {
        email,
      },
    });

    return userData;
  }

  static async checkDuplicateUsername(username) {
    const userData = dbUser.findOne({
      where: {
        username,
      },
    });

    return userData;
  }

  static async checkDuplicatePhoneNumber(phoneNumber) {
    const userData = dbUser.findOne({
      where: {
        phone_number: phoneNumber,
      },
    });

    return userData;
  }

  static async getUserList(
    page = 1,
    row = 10,
    orderBy = 'created_at',
    orderType = 'DESC'
  ) {
    const pagination = misc.simplePagination(page, row);

    return dbUser.findAndCountAll({
      offset: pagination.page,
      limit: pagination.row,
      order: [[orderBy, orderType]],
    });
  }

  static async registerUser(data) {

    let t = await sequelize.transaction();

    try {
      const objectUser = {
        name: data.name,
        email: data.email,
        username: data.username,
        password: misc.passwordHash(data.password),
        birth_date: data.birth_date,
        gender: data.gender,
      };

      if (data.birth_date) {
        objectUser.birth_date = data.birth_date;
      }
      if (data.gender) {
        objectUser.gender = data.gender;
      }
      if (data.phone_number) {
        objectUser.phone_number = data.phone_number;
      }
      const userData = await dbUser.create(objectUser, { transaction: t }).catch((err) => {
        throw new Error(err);
      });

      const dataToken = {
        user_id: userData.dataValues.user_id,
        name: userData.dataValues.name,
        username: userData.dataValues.username,
        email: userData.dataValues.email,
        gender: userData.dataValues.gender,
      };

      const token = await initialHelper.generateJWT(dataToken, '365d');

      const payload = {
        access_token: token,
        user_id: userData.dataValues.user_id,
        name: userData.dataValues.name,
        username: userData.dataValues.username,
        email: userData.dataValues.email,
        gender: userData.dataValues.gender,
      };
      t.commit()
      return payload;
    } catch (e) {
      t.rollback()
      throw new Error(err);
    }
  }


  static async login(data = '', password) {
    let userData;

    if (validator.isEmail(data)) {
      userData = await this.findUserByEmail(data);
    }
    if (!userData) {
      return null;
    }
    const checkPass = await misc.passwordCheck(password, userData.password);

    if (checkPass === true) {
      const sendJwt = await initialHelper.generateJWT(
        {
          user_id: userData.user_id,
          name: userData.name,
          username: userData.username,
          email: userData.email,
          gender: userData.gender,
        },
        '365d'
      );

      return sendJwt;
    }
    return null;
  }

  static async findUserByEmail(userEmail) {
    const userData = await dbUser
      .findOne({
        where: {
          email: userEmail,
        },
        raw: true,
      })
      .catch((err) => {
        throw new Error(err);
      });

    return userData;
  }

  static async findUserByUsername(username) {
    const userData = await dbUser
      .findOne({
        where: {
          username,
        },
        raw: true,
      })
      .catch((err) => {
        throw new Error(err);
      });

    return userData;
  }

  static async findUserByPhoneNumber(phoneNumber) {
    const userData = await dbUser
      .findOne({
        where: {
          phone_number: phoneNumber,
        },
        raw: true,
      })
      .catch((err) => {
        throw new Error(err);
      });

    return userData;
  }

  static async findUserByIdJwt(userId) {
    const userData = await dbUser
      .findOne({
        where: {
          user_id: userId,
        },
        raw: true,
      })
      .catch((err) => {
        throw new Error(err);
      });

    return userData;
  }

  async updateUser(data) {
    const userUpdate = {};

    if (data.name) {
      userUpdate.name = data.name;
    }
    if (data.username) {
      userUpdate.username = data.username;
    }
    if (data.gender) {
      userUpdate.gender = data.gender;
    }
    if (data.birth_date) {
      userUpdate.birth_date = data.birth_date;
    }
    if (data.description) {
      userUpdate.description = data.description;
    }
    if (data.email) {
      userUpdate.email = data.email;
    }

    const userData = await this.findUserById();

    if (data.password) {
      if (data.current_password) {
        const passwordCheck = misc.passwordCheck(
          data.current_password,
          userData.password
        );

        if (!passwordCheck) {
          return null;
        }
      }

      userUpdate.password = await misc.passwordHash(data.password);
    }

    const updatedData = await userData.update(userUpdate, { returning: true });

    const sendJwt = await initialHelper.generateJWT(
      {
        user_id: updatedData.user_id,
        name: updatedData.name,
        username: updatedData.username,
        email: updatedData.email,
        gender: updatedData.gender,
      },
      '365d'
    );

    return sendJwt;
  }

  async deleteUser() {

    const deleteUser = await dbUser.destroy({
      where: {
        user_id: this.userId,
      }
    });
    return deleteUser;
  }
}

export default User;
