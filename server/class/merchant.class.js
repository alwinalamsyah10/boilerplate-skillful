import crypto from 'crypto';
import { Op, where } from 'sequelize';
import { sequelize } from '../models';
import validator from 'validator';
import moment, { duration } from 'moment';
import initialHelper from './util/initialization.class';
import misc from '../misc/misc';


require('dotenv').config();

const {
  user: dbUser,
  merchant: dbMerchant,
} = require('../models/index');

class Merchant {
  constructor(merchantId) {
    this.merchantId = merchantId;
  }

  async findMerchantById() {
    const userData = await dbMerchant
      .findOne({
        where: {
          merchant_id: this.merchantId,
        },
        include: [
          {
            model: dbUser
          }
        ]
      })
      .catch((err) => {
        throw new Error(err);
      });

    return userData;
  }

  static async addMerchant(data, userId) {

    let t = await sequelize.transaction();

    try {
      const objectmerchant = {
        name: data.name,
        description: data.description,
        user_id: userId
      };
      const merchantData = await dbMerchant.create(objectmerchant, { transaction: t }).catch((err) => {
        throw new Error(err);
      });

      t.commit()
      return merchantData;
    } catch (e) {
      t.rollback()
      throw new Error(err);
    }
  }
}

export default Merchant;
