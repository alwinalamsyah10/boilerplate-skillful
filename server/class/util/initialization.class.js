import jwt from 'jsonwebtoken';

require('dotenv').config();

class Initialization {
  constructor(token) {
    this.token = token;
  }

  static generateJWT(userData, expireTime = '365d') {
    return jwt.sign(userData, process.env.JWT_TOKEN, {
      expiresIn: expireTime,
    });
  }

  getJwtData() {
    return jwt.verify(this.token, process.env.JWT_TOKEN);
  }

  static getUserJWT(input) {
    const userData = input.replace('Bearer ', '');

    return jwt.verify(userData, process.env.JWT_TOKEN);
  }
}

export default Initialization;
