/* eslint-disable no-unused-vars */
import User from '../class/user.class';
import Merchant from '../class/merchant.class';
import Response from '../class/util/response.class';
require('dotenv').config();

async function getMerchantData(req, res) {
  const response = new Response(res);
  if (!req.params.id) {
    return response.contentFail(403, 'Forbidden', 'FO001');
  }
  const merchant = new Merchant(req.params.id);

  const merchantData = await merchant.findMerchantById();

  if (!merchantData) {
    return response.contentFail(404, 'Merchant Not Found', 'NF001');
  }

  return response.contentSuccess(200, merchantData);
}


async function addMerchant(req, res) {
  const response = new Response(res);

  if (!req.headers.authorization) {
    return response.contentFail(401, 'Unauthorized', 'UN001');
  }
  const userJwt = await User.getUserDataJWT(
    req.headers.authorization,
    res
  );

  const data = await Merchant.addMerchant(req.body, userJwt.user_id);

  return response.contentSuccess(200, data);
}

export default {
  addMerchant,
  getMerchantData
};
