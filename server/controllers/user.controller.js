/* eslint-disable no-unused-vars */
import User from '../class/user.class';
import Response from '../class/util/response.class';
require('dotenv').config();

async function userRegister(req, res) {
  const response = new Response(res);

  const duplicateEmail = await User.checkDuplicateEmail(req.body.email);
  if (duplicateEmail) {
    return response.contentFail(400, 'Duplicate email', 'US001');
  }

  if (req.body.phone_number) {
    const duplicatePhoneNumber = await User.checkDuplicatePhoneNumber(
      req.body.phone_number
    );
    if (duplicatePhoneNumber) {
      return response.contentFail(400, 'Duplicate phone number', 'US002');
    }
  }

  const duplicateUsername = await User.checkDuplicateUsername(
    req.body.username
  );
  if (duplicateUsername) {
    return response.contentFail(400, 'Duplicate username', 'US003');
  }

  const dataUser = await User.registerUser(req.body);

  return response.contentSuccess(200, dataUser);
}

async function login(req, res) {
  const { input, password } = req.body;

  const loginValidation = await User.login(input, password);
  const response = new Response(res);
  if (loginValidation) {
    return response.contentSuccess(200, loginValidation);
  }
  return response.contentFail(400, 'Incorrect email or password', 'AU001');
}

async function getAllUser(req, res) {
  const { page, row, order_by: orderBy, order_type: orderType } = req.query;
  const response = new Response(res);
  const userList = await User.getUserList(page, row, orderBy, orderType);

  return response.contentSuccess(200, userList);
}


async function getUserData(req, res) {
  const response = new Response(res);
  if (!req.headers.authorization) {
    return response.contentFail(401, 'Unauthorized', 'UN001');
  }
  const userJwt = await User.getUserDataJWT(
    req.headers.authorization,
    res
  );

  const user = new User(userJwt.user_id);

  const userData = await user.findUserById();

  if (!userData) {
    return response.contentFail(404, 'User Not Found', 'NF001');
  }

  return response.contentSuccess(200, userData);
}

async function updateUserData(req, res) {
  const response = new Response(res);
  if (!req.headers.authorization) {
    return response.contentFail(401, 'Unauthorized', 'UN001');
  }
  const dataJwt = await User.getUserDataJWT(
    req.headers.authorization,
    res
  );

  const user = new User(dataJwt.user_id);

  if (req.body.email) {
    const duplicateEmail = await User.checkDuplicateEmail(req.body.email);
    if (duplicateEmail) {
      return response.contentFail(400, 'Duplicate email', 'US001');
    }
  }
  if (req.body.username) {
    const duplicateUsername = await User.checkDuplicateUsername(
      req.body.username
    );
    if (duplicateUsername) {
      return response.contentFail(400, 'Duplicate username', 'US003');
    }
  }

  const updateDataUser = await user.updateUser(req.body);

  return response.contentSuccess(200, updateDataUser);
}

async function deleteUserData(req, res) {
  const response = new Response(res);

  const user = new User(req.params.id);

  const userData = await user.findUserById();

  if (!userData) {
    return response.contentFail(404, 'User Not Found', 'NF001');
  }

  const deleteDataUser = await user.deleteUser();

  return response.contentSuccess(200, deleteDataUser);
}

export default {
  userRegister,
  deleteUserData,
  login,
  getAllUser,
  getUserData,
  updateUserData,
};
