import express from 'express';
import userRoute from './user.routes';
import merchantRoute from './merchant.routes';


const router = express.Router();

router.use('/user', userRoute);
router.use('/merchant', merchantRoute);

export default router;
