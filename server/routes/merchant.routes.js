import express from 'express';
import merchantController from '../controllers/merchant.controller';

const router = express.Router();

router.post('/add', merchantController.addMerchant);
router.get('/:id', merchantController.getMerchantData);

export default router;
