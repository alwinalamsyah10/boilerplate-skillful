import express from 'express';
import userController from '../controllers/user.controller';

const router = express.Router();

router.post('/register', userController.userRegister);
router.get('/', userController.getUserData);
router.post('/login', userController.login);
router.get('/list', userController.getAllUser);
router.put('/detail', userController.updateUserData);
router.delete('/delete/:id', userController.deleteUserData);


export default router;
