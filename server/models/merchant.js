const { Model } = require('sequelize');

module.exports = (sequelize, DataTypes) => {
  class merchant extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(model) {
      merchant.belongsTo(model.user, {
        foreignKey: 'user_id',
        targetKey: 'user_id'
      });
    }
  }
  merchant.init(
    {
      merchant_id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.BIGINT,
      },
      name: DataTypes.STRING,
      description: DataTypes.TEXT,
      user_id: DataTypes.BIGINT,
    },
    {
      sequelize,
      modelName: 'merchant',
      underscored: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',
      deletedAt: 'deleted_at',
      paranoid: true,
    }
  );
  return merchant;
};
