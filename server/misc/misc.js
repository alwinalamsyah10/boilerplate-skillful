import bcrypt from "bcryptjs";
import crypto from 'crypto';
import fs from 'fs';

require("dotenv").config();

const simplePagination = (page = 1, row = 10) => {
  const pagination = {
    page,
    row,
  };
  if (row) {
    pagination.row = row;
  }
  if (page) {
    pagination.page = (page - 1) * pagination.row;
  }
  return pagination;
};

const passwordHash = (password) => {
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);

  return hash;
};

const passwordCheck = (inputPass = " ", hashPass) => {
  const check = bcrypt.compareSync(inputPass, hashPass);
  return check;
};

const hashGenerator = async (data) => {
  const hash = crypto.createHash('sha512');
  hash.update(data);
  const value = hash.digest('hex');
  return value;
};

export default {
  simplePagination,
  passwordHash,
  passwordCheck,
  hashGenerator,
};
