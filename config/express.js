import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
// import formidable from 'formidable';
import formData from 'express-form-data';
import fs from 'fs';
import route from '../server/routes';
import Response from '../server/class/util/response.class';

require('dotenv').config();

const app = express();

const baseUrl = '/api';

app.use(cors({
  preflightContinue: true,
  methods: 'GET,PUT,POST,DELETE,OPTIONS,HEAD,PATCH',
  origin: '*',
  allowedHeaders: 'Content-Type, Origin, Authorization, Accept',
  credentials: true,
}));

const options = {
  uploadDir: `${__dirname}/../upload_media`,
  autoClean: false,
};

app.use((req, res, next) => {
  return next();
});

// parse data with connect-multiparty.
app.use(formData.parse(options));
// delete from the request all empty files (size == 0)
app.use(formData.format());
// change the file objects to fs.ReadStream
app.use(formData.stream());
// union the body and the files
app.use(formData.union());

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(baseUrl, route);

app.use((err, req, res, next) => {
  console.info(err);

  return next(err.stack);
});

// error handler, send stacktrace only during development
app.use((err, _, res, next) => {
  if (err) {
    const response = new Response(res);

    return response.systemError(err);
  }
  return next();
});

// catch 404 and forward to error handler
app.use((_, res) => {
  const response = new Response(res);

  return response.apiNotFound();
});

export default app;
